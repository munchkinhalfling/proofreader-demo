import proofreader from "@munchkinhalfling/proofreader";

const yaml = document.querySelector("#code");
const result = document.querySelector("#result");
const html = document.querySelector("#html");

yaml.addEventListener("keyup", () => {
    try {
        result.childNodes.forEach(node => {
            result.removeChild(node);
        });
        result.appendChild(proofreader(yaml.value));
        html.textContent = result.innerHTML;
    } catch(e) {
        console.log(e);
    }
});